import React, { Component } from "react";
import List from "./List";
import * as Helper from "../Helper";

import "./App.scss";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: "",
      openItems: [],
      doneItems: [],
      maxLen: 20,
      dataFetched: false
    };
  }

  // componentDidMount() {
  //   this.fetchData();
  // }

  fetchData = () => {
    if (!this.state.dataFetched) {
      fetch("https://jsonplaceholder.typicode.com/todos")
        .then(response => response.json())
        .then(data =>
          data.map(data =>
            this.setState({
              openItems: [...this.state.openItems, data.title],
              dataFetched: true
            })
          )
        )
        .then(
          Helper.pushNotify(
            "Data fetched successfully!",
            "Success!",
            "logo96.png"
          )
        );
    }
  };

  onChange = event => {
    this.setState({ term: event.target.value });
  };

  onSubmit = event => {
    event.preventDefault();
    this.setState({
      term: "",
      openItems: [...this.state.openItems, this.state.term]
    });
    Helper.pushNotify(
      this.state.term + " added to the list!",
      "Success!",
      "logo96.png"
    );
  };

  overflowAlert = () => {
    if (this.getRemainingChars() < 0) {
      return (
        <div className="alert alert-warning text-left mt-3 mb-0">
          Warning: Text Too Long
        </div>
      );
    }
    return "";
  };

  getRemainingChars = () => {
    let chars = this.state.maxLen - this.state.term.length;
    return chars;
  };

  handleDone = id => {
    const remainderList = this.state.openItems.filter((item, index) => {
      if (index !== id) {
        return item;
      } else {
        Helper.pushNotify(
          item + " marked done successfully!",
          "Completed!",
          "logo96.png"
        );
        return null;
      }
    });
    const doneList = this.state.openItems.filter((item, index) => index === id);
    this.setState({
      openItems: remainderList,
      doneItems: [...this.state.doneItems, doneList]
    });
  };

  handleRemove = (id, active) => {
    let itemClicked;
    active === "open"
      ? (itemClicked = this.state.openItems)
      : (itemClicked = this.state.doneItems);
    const remainder = itemClicked.filter((item, index) => {
      if (index !== id) {
        return item;
      } else {
        Helper.pushNotify(
          item + " deleted successfully!",
          "Deleted!",
          "logo96.png"
        );
        return null;
      }
    });
    active === "open"
      ? this.setState({ openItems: remainder })
      : this.setState({ doneItems: remainder });
  };

  render() {
    return (
      <main className="">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md">
              <div className="card shadow-sm mb-3">
                <div className="card-body">
                  <h4 className="card-title">Add Tasks</h4>
                  <form onSubmit={this.onSubmit}>
                    <input
                      type="text"
                      className="form-control mb-3"
                      value={this.state.term}
                      onChange={this.onChange}
                      autoFocus
                    />
                    <button
                      className="btn btn-success mr-2"
                      disabled={
                        this.state.term.length === 0 ||
                        this.state.term.length > this.state.maxLen
                      }
                    >
                      Add
                    </button>
                    {/* <button
                  type="button"
                  className="btn btn-secondary"
                  disabled={this.state.dataFetched}
                  onClick={this.fetchData}
                >
                  Fetch Notes
                </button> */}
                    <span className="btn float-right disabled">
                      {this.state.maxLen - this.state.term.length}
                    </span>
                  </form>
                  {this.overflowAlert()}
                </div>
              </div>
            </div>
            <div className="col-md-6 col-xl-4">
              <List
                title={"Open Tasks"}
                items={this.state.openItems}
                btn1={this.handleDone}
                btn2={e => this.handleRemove(e, "open")}
              />
            </div>
            <div className="col-md-6 col-xl-4">
              <List
                title={"Completed Tasks"}
                items={this.state.doneItems}
                btn1={this.handleDone}
                btn2={e => this.handleRemove(e, "done")}
              />
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default App;
