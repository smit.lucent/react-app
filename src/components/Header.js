import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <h1 className="h5 my-3 text-center text-white">
        Hello from another DOM!
      </h1>
    );
  }
}

export default Header;
